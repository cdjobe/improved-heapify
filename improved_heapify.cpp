// 	Carson Jobe - Data Structures and Algorithms - Homework 4
// 	Due March 13, 2015

/* 	Design an implement an efficient HeapifyDownward algorithm
	that reduces the number of data/key item comparisons to
	about log2(n) in the worst case. Note: No credit will be
	given to the implementations that do not satisfy the
	running time constraint.
	Apply the efficient HeapifyDownward to implement the
	Heapsort algorithm
*/

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <math.h>

using std::cout;
using std::endl;


//Definitions
const int SIZE = 20;


int* genNum();
void ImprovedHeapSort(int A[], int arraySize);
void ImprovedBuildMaxHeap(int A[], int arraySize);
void ImprovedHeapifyDownward(int A[], int k, int arraySize);
int search(int A[], int k, int i,int copy, int arraySize);
int findMid(int k,int i, int arraySize);
int pathSize(int k, int i, int arraySize);
int findLeaf(int A[], int i, int arraySize);




/********************************************************************/
// Improved Heap f
/********************************************************************/
/********************************************************************/
int Parent(int i) {
	//cout << "Parent" << endl;
	return(floor((i-1)/2));
}

int Left(int i) {
	//cout << "Left" << endl;
	return(2*i+1);
}

int Right(int i) {
	//cout << "Right" << endl;
	return(2*i+2);
}

/*	findLeaf
	creates a path of largest children from the supplied index i to the
	returned value leaf.

	There is only one node to node value comparison.
	There are two node index comparisons to guarantee that only nodes
	within the tree are returned.
*/

int findLeaf(int A[], int i, int arraySize) {

	// Variables
	int r = Right(i);
	int l = Left(i);
	int leaf = i;

	/*	If the left child's index is less than the SIZE (number of
		nodes, then it is safe to continue. The difference between
		l and SIZE will be >= 1 */

	if (l < arraySize) {
		cout << "#####FIND LEAF#####" << endl;
 		cout << "Parent node= " << i << endl;
		cout << "l= " << l << endl;
		cout << "r= " << r << endl;

		if ((A[l] < A[r]) && (r < arraySize)) {
			//cout << "first i =  " << i << endl;
			leaf = findLeaf(A, r, arraySize);
			//cout << "after first i =  " << i << endl;
		}
		else {
			//cout << "Second i= " << i << endl;
			leaf = findLeaf(A,l, arraySize);
			//cout << "after Second i= " << i << endl;
		}
	}

	//cout << "Sending leaf = " << leaf << endl;
	return leaf;

}

/* pathSize
Returns the pathSize (shared edges) between two nodes.
(+1) offset to indicate which node/SIZE rather than index.
k = higher on tree
i = lower "..."
*/
int pathSize(int k, int i, int arraySize) 	{
	return abs(floor(log2(k+1)) - floor(log2(i+1)));
}
/*	findMid
	returns the index of middle ancestor from leaf to root.
	Starts at leaf, determines the number of edges between
	leaf and root, continues until the floor(half) of them
	have been traversed.
*/
int findMid(int k,int i, int arraySize) {
	// Finds the length of numbers to be searched based on the placement of each node
	// offset index with +1 to indicate node
	int len = pathSize(k, i, arraySize);
	cout << "Pathsize = " << len << endl;

	// j = 0, j will increase until j = pathSize and i=mid index
	// j goes "up" a level until it has reached the mid index given by pathsize/2
	for (int j = 0; j < floor((len/2)); j++) {
		//cout << "findMid" << endl;
		i = Parent(i);
	}

	return i;
}

/*	Search
	will search from a specified leaf(i) up to a specified root(k)
	should return the index which the new value should go and all ancestor nodes should be moved up.
	This index should be the index whose value in the array is larger than copy by the least amount.
	In a descending array:
		If copy < mid: search right side of array (where lower values are)
		If copy > mid: search left side of array (...higher values)
		Continue until the correct index is found.
	Since it is a binary search, the comparisons are expected to be half that of regular heapsort, with
	performance increasing with larger volumes of input.
*/
int search(int A[], int k, int i, int copy, int arraySize) {
	cout << "#####SEARCH#####" <<endl<< endl;
	// k = the index located higher on the tree, but lower index value
	// i = the index located lower on the tree, but higher index value

	int mid = findMid(k, i, arraySize);

	// Print some stuff
	cout << "k = " << k << " i = " << i << endl;
	cout << "mid = " << mid << endl;
	//cout << "Parent = " << Parent(mid) << endl;
	cout << "A[mid]= " << A[mid] << endl;
	cout << "Copy = " << copy << endl <<endl;

	/*	if k(root higher on tree, but lower index) >= i(lower root, higher index)
		then only one node is being examined.

		This must be checked first, so that the recursion calls
		can end.
	*/
	if (k >= i) {
		cout << "Set is empty..." << endl;
		cout << "Return k = " << k << endl;
		return k;
	}
	else {
		if (copy < A[mid]) { // Search right side of array, with middle index being sent as k
			mid = search(A, mid, i, copy, arraySize);
		}
		else if (copy > A[mid]) // Search left side of array, from k to the parent of A[mid]
			mid = search(A, k, Parent(mid), copy, arraySize);
		}
}

/*	Improved Heapify Downward
	This will create a path of the largest children down to the
	leaves of the tree. (findLeaf())

	A binary search will be run from the leaves up to determine
	the placement of the old root. (search())

	From there, the values will be moved up according to the number
	of nodes/edges between k and leaf.

	In original Max-Heapify, recursive calls were made to itself, to ensure
	that the heap was maintained. However, with the binary search function
	search, the heap is maintained with help from the findLeaf function.

	ImprovedHeapifyDownward -
	Starts from an index provided by ImprovedBuildMaxHeap (which is the middle node).
	From k, findLeaf finds the path of largest children created until there are no more children.
	A copy is made of A[k].
	Search finds the correct placement of k, by binary search.
	And moves all values up the correct number of times until the value at A[k] has been replaced.
	Finally, the original value is put in its correct place.

*/

void ImprovedHeapifyDownward(int A[], int k, int arraySize) {
	cout << "#####Improved Heapify Downward#####"<<endl;

	//
	int leaf = findLeaf(A, k, arraySize); // returns leaf of the largest path
	cout << "Leaf = " << leaf << endl <<endl;

	// Once at base of tree, binary search up to k from leaf
	int tempCopy = A[k];
	int copyIndex;
	copyIndex = search(A, k, leaf, tempCopy, arraySize);

	cout << "#####Copying Values#####" << endl;
	cout << "copyIndex = " << copyIndex << endl;
	//cout << "k = " << k << endl;

	// copy values from mid up to k
	// copy the parent, move child up, replace j's grandparent, increase j
	int len = pathSize(k, copyIndex, arraySize);
	int node = copyIndex;
	int temp = A[node];
	int temp2 = A[Parent(node)];

	for(int j = 1; j <= len; j++) {
		cout << "j = " << j << endl;
		A[Parent(node)]= temp;
		temp = temp2;
		temp2 = A[Parent(Parent(node))];
		node = Parent(node);
	}

	// Place copy into it's Spot

	A[copyIndex] = tempCopy;

	cout << endl << "#####Print Max Heap#####" << endl;
	for (int i = 0; i <= SIZE-1; i++) {
		cout << "A["<< i << "] = " << A[i] << " " << endl;
	}
	cout <<endl<<endl;

}

/*	ImprovedBuildMaxHeap
	like regular MaxHeap, but calls ImprovedHeapifyDownward
*/
void ImprovedBuildMaxHeap(int A[], int arraySize) {

	// Variables
	int len = floor(SIZE/2);

	char space;


	// Statements
	cout << "Improved BUILD MAX HEAP" << endl;


	for (int i = len; i >= 0; --i) {
		cout << endl;
		cout << "#############################################" << endl;
		cout << "#############################################" << endl;
		cout << endl;
		cout << "Improved BUILD MAX HEAP i= " << i << endl;
		ImprovedHeapifyDownward(A, i, arraySize);
		//cout << "Improved AFTER HEAPIFY DOWNWARD i = " << i << endl;
		//std::cin >> space;
	}
	cout << "End of build max heap"<<endl;
}

/*	ImprovedHeapSort
	like original HeapSort, but calls ImprovedBuildMaxHeap, and
	ImprovedHeapifyDownward.
*/

void ImprovedHeapSort(int A[], int arraySize) {

	// Variables
    char space;

	cout << "Improved HeapSort" << endl;
	ImprovedBuildMaxHeap(A, arraySize);
	//cout << "done building improved heap" << endl;

	arraySize = arraySize -1;
	for (int j = SIZE-1; j>= 2; j--) {
		cout << "##########HEAPSORT##########" << endl;
		int temp = A[j];
		cout << "Temp = " << A[j] << endl;
		A[j] = A[0];
		cout << "A[j] = " << A[0] << endl;
		A[0] = temp;
		cout << "A[0] = " << temp << endl;
		cout << "Improved Heapsort j = " << j << endl;
		arraySize = arraySize - 1;
		cout << "!!!!!!!!!!!!!!ARRAY SIZE!!!!!!!!!!!!!!! = " << arraySize << endl;
		ImprovedHeapifyDownward(A, 0, arraySize);
		//std::cin >> space;
	}
}
/********************************************************************/
/********************************************************************/
/********************************************************************/
int* genNum() {
	int* numbers = new int[SIZE];

	cout << "GEN NUM" << endl;

	for(int i = 0; i < SIZE; i++){
		numbers[i] = rand() % 101 + 1;
		cout << numbers[i] << " ";
	}
	cout << endl;
	return numbers;
}
/********************************************************************/

int main() {
	// random seed
	srand(time(NULL));

	// Variables

	int arraySize = SIZE;
 	int* nums2;
	nums2 = genNum();

	cout << endl << "IMPROVED HEAPSORT" << endl;
	ImprovedHeapSort(nums2, arraySize);

	for (int i = 0; i <= arraySize - 1; i++) {
		cout << "A["<< i << "] = " << nums2[i] << " " << endl;
	}

	/*int test[20] = {3,28,32,26,28,5,13,23,42,94,36,32,33,10,100,47,14,97,88,37};
	cout << endl << "#####Print Max Heap#####" << endl;
	for (int i = 0; i <= SIZE-1; i++) {
		cout << "A["<< i << "] = " << test[i] << " " << endl;
	}
	ImprovedHeapSort(test, arraySize);
	for (int i = 0; i < SIZE; i++){
		cout << test[i] << " ";
	}*/
    delete(nums2);
    return 0;

}
